#!/bin/sh

set -eu

FB_VERSION=1.5.4

FB_VER="1.5"
FB2="firebird$FB_VER"
FB2_no_dots=`echo $FB2 | sed -e 's/\.//g'`
FB2DIR="firebird/$FB_VER"
ULFB2="usr/lib/$FB2DIR"
USFB2="usr/share/$FB2DIR"
VAR="var/lib/$FB2DIR"
SO_VER=1


copy ()
{
    type=$1
    dest=$2
    shift
    shift

    case "$type" in
        e*) mode="755" ;;
        f*) mode="644" ;;
        *) echo "Error: Wrong params for copy!"; exit 1;;
    esac

    install -m $mode "$@" "$dest"
}

# Helper function used both in -super and -classic
copy_utils()
{
    for s in gbak gdef gfix gpre qli gsec gstat isql ;
    do
        target=$s
        if [ $target = gstat ];
        then
            target=fbstat
        elif [ $target = isql ];
        then
            target=isql-fb
        fi

        copy e $D/usr/bin/$target-$FB_VER $S/bin/$s

        # create links brutally. dh_link will sanitize them
        ln -s /usr/bin/$target-$FB_VER $D/$ULFB2/bin/$s
    done
}

#-super
make_super () {
    P_name="$FB2-super"
    echo "Creating $P_name content"
    D=debian/$P_name
    S=debian/firebird2-super

    mkdir -p $D/usr/bin $D/$ULFB2/bin $D/$ULFB2/UDF

    copy e $D/$ULFB2/bin $S/bin/fb_lock_print

    copy e $D/$ULFB2/bin $S/bin/fbserver \
	$S/bin/fbguard \
	$S/bin/fbmgr.bin

    copy e $D/$ULFB2/UDF $S/UDF/fbudf.so $S/UDF/ib_udf.so

    mkdir -p $D/usr/bin
    mkdir -p $D/$ULFB2/bin

    copy_utils

    copy e $D/$ULFB2/bin debian/fbmgr

    # lintian override
    mkdir -p $D/usr/share/lintian/overrides
    copy f $D/usr/share/lintian/overrides/$P_name \
      debian/$P_name.lintian.overrides

    # linda overrides
    mkdir -p $D/usr/share/linda/overrides
    copy f $D/usr/share/linda/overrides/$P_name \
      debian/$P_name.linda.overrides
}

#-classic
make_classic () {
    P_name="$FB2-classic"
    echo "Creating $P_name content"
    D=debian/$P_name
    S=debian/firebird2-classic

    mkdir -p $D/$ULFB2/bin      \
            $D/$ULFB2/misc      \
            $D/$ULFB2/UDF       \
            $D/usr/bin          \
            $D/etc/xinetd.d
    mkdir -p $D/usr/bin
    mkdir -p $D/$ULFB2/bin

    copy e $D/$ULFB2/bin $S/bin/fb_inet_server \
	$S/bin/fb_lock_mgr \
	$S/bin/fb_lock_print \
	$S/bin/gds_drop

    copy_utils

    install -m 0644 debian/$FB2-classic.xinetd \
    		    $D/etc/xinetd.d/$FB2_no_dots

    copy e $D/$ULFB2/UDF $S/UDF/fbudf.so $S/UDF/ib_udf.so

    # lintian overrides
    mkdir -p $D/usr/share/lintian/overrides
    copy f $D/usr/share/lintian/overrides/$P_name \
      debian/$P_name.lintian.overrides

    # linda overrides
    mkdir -p $D/usr/share/linda/overrides
    copy f $D/usr/share/linda/overrides/$P_name \
      debian/$P_name.linda.overrides
}

#libfbclient
make_libfbclient () {
    P="libfbclient$SO_VER"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird2-super

    mkdir -p $D/usr/lib

    copy e $D/usr/lib $S/lib/libfbclient.so.$FB_VERSION
    ln -s libfbclient.so.$FB_VERSION $D/usr/lib/libfbclient.so.$SO_VER
}

#libfbembed
make_libfbembed () {
    P="libfbembed$SO_VER"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird2-classic

    mkdir -p $D/usr/lib

    copy e $D/usr/lib $S/lib/libfbembed.so.$FB_VERSION
    ln -s libfbembed.so.$FB_VERSION $D/usr/lib/libfbembed.so.$SO_VER
}


#-common
make_common () {
    P="$FB2-common"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird2-super

    mkdir -p $D/etc/$FB2DIR \
        $D/usr/share/doc/$P \
        $D/etc/logrotate.d \
        $D/$ULFB2 \
        $D/$USFB2

    mkdir -p \
        $D/$ULFB2/UDF \
        $D/$ULFB2/lib $D/$ULFB2/intl \
        $D/var/run/$FB2DIR \
        $D/$VAR \
        $D/$VAR/system \
        $D/$VAR/tmp \
        $D/$VAR/data \
        $D/$VAR/backup \
        $D/usr/share/doc/$P \
        $D/usr/share/$P \
        $D/etc/$FB2DIR

    install -m 0644 -o root -g root \
        debian/$P.logrotate \
        $D/etc/logrotate.d/$FB2

    for m in $S/*.msg;
    do
        copy f $D/$USFB2 $m
    done

    # config, log
    copy f $D/etc/$FB2DIR $S/install/misc/firebird.conf \
        $S/install/misc/aliases.conf

    # fix aliases.conf: employee.fdb should point to a database
    # in /$VAR/data where all databases live
    sed -e 's/\/usr\/lib\/firebird\/2.0\/examples\/empbuild/\/var\/lib\/firebird\/2.0\/data/' \
    < $D/etc/$FB2DIR/aliases.conf > $D/etc/$FB2DIR/aliases.conf.new
    mv $D/etc/$FB2DIR/aliases.conf.new $D/etc/$FB2DIR/aliases.conf

    install -m 0644 -o root -g root \
        debian/functions.sh \
        $D/usr/share/$P/

    touch $D/$VAR/backup/no_empty
    touch $D/$VAR/data/no_empty

    copy f $D/$ULFB2/UDF \
        src/extlib/fbudf/fbudf.sql \
        src/extlib/ib_udf.sql

    copy e $D/$ULFB2/lib $S/lib/libib_util.so
    install -m 0755 $S/intl/libfbintl.so $D/$ULFB2/intl/fbintl

    # databases
    cp $S/security.fdb \
    $D/$VAR/system/default-security.fdb

    copy f $D/$VAR/system $S/help/help.fdb

    # manpages
    for u in fbstat gbak gdef gsec isql-fb gfix gpre qli;
    do
        cp debian/$u.1 debian/$u-$FB_VER.1
        dh_installman -p $P debian/$u-$FB_VER.1
        rm debian/$u-$FB_VER.1
    done

    #lintian override
    mkdir -p $D/usr/share/lintian/overrides
    copy f $D/usr/share/lintian/overrides/$P \
      debian/$P.lintian.override
}

#-dev
make_dev () {
    P="$FB2-dev"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird2-super

    mkdir -p $D/usr/include
    mkdir -p $D/$ULFB2/lib

    copy f $D/usr/include $S/include/*.h

    ln -s libfbclient.so.$FB_VERSION $D/usr/lib/libfbclient.so
    ln -s libfbembed.so.$FB_VERSION $D/usr/lib/libfbembed.so
}


#-examples
make_examples () {
    P="$FB2-examples"
    echo "Creating $P content"
    D=debian/$P
    S=debian/firebird2-super

    mkdir -p $D/usr/share/doc/$P/examples
    mkdir -p $D/$ULFB2
    cp -r $S/examples $D/usr/share/doc/$P/

    install -m 0644 \
        debian/$P.README.Debian \
        $D/usr/share/doc/$P/README.Debian
}

#-doc
make_doc () {
    P="$FB2-doc"
    echo "Creating $P content"
    D=debian/$P
    S=doc

    mkdir -p $D/usr/share/doc/$P

    cp -r $S $D/usr/share/doc/$P/docs
}

umask 022
make_super
make_classic
make_libfbclient
make_libfbembed
make_common
make_dev
make_examples
make_doc
echo "Packages ready."
exit 0;

