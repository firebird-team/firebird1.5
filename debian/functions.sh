# Shell functions library used by firebird1.5-{super,classic}.postinst
# This file needs to be sourced

if [ -z "${FB_VER:-}" ];
then
    echo Please define FB_VER before sourcing functions.sh
    exit 1
fi

if [ -z "${FB_FLAVOUR:-}" ];
then
    echo Please define FB_FLAVOUR before sourcing functions.sh
    exit 1
fi

export FB_VER
FB_VER_no_dots=`echo $FB_VER | sed -e 's/\.//g'`

FB="/usr/lib/firebird/$FB_VER"
VAR="/var/lib/firebird/$FB_VER"
ETC="/etc/firebird/$FB_VER"
LOG="/var/log/firebird${FB_VER}.log"
RUN="/var/run/firebird/$FB_VER"
DEFAULT="/etc/default/firebird${FB_VER}-${FB_FLAVOUR}"
DBAPasswordFile="$ETC/SYSDBA.password"

fixPermsFirstInstall()
{
    find $VAR -type d -exec chown firebird:firebird {} \; \
                           -exec chmod 0770 {} \;
    find $VAR -type f -exec chown firebird:firebird {} \; \
                           -exec chmod 0660 {} \;

    if [ ! -f $LOG ]; then
        touch $LOG
        chown -f firebird:firebird $LOG
        chmod -f 660 $LOG
    fi
}

fixPermsConfigure()
{
    find $RUN -type d \
            -exec chown firebird:firebird {} \; \
            -exec chmod 0770 {} \;
    find $RUN -type f \
            -exec chown firebird:firebird {} \; \
            -exec chmod 0660 {} \;

}

#---------------------------------------------------------------------------
# set new SYSDBA password with gsec

writeNewPassword () {
    local NewPasswd=$1
    local VER=$2

    local DBAPasswordFile="/etc/firebird/$VER/SYSDBA.password"

    # Provide default SYSDBA.password
    if [ ! -e "$DBAPasswordFile" ];
    then
        touch "$DBAPasswordFile"
        chmod 0600 $DBAPasswordFile

        cat <<_EOF > "$DBAPasswordFile"
# Password for firebird SYSDBA user
#
# You may want to use the following commands for changing it:
#   dpkg-reconfigure firebird${VER}-super
# or
#   dpkg-reconfigure firebird${VER}-classic
#
# If you change the password manually with gsec, please update it here too.
# Keeping this file in sync with the security database is critical for the
# correct functioning of the init.d script and for the ability to change the
# password via \`dpkg-reconfigure firebird${VER}-super/classic\'

ISC_USER=sysdba
ISC_PASSWORD=masterkey
_EOF
        ISC_PASSWORD=masterkey
    else
        . $DBAPasswordFile
    fi
    if [ "$NewPasswd" != "$ISC_PASSWORD" ]; then
        export ISC_PASSWORD
        gsec-$VER -user sysdba <<EOF
modify sysdba -pw $NewPasswd
EOF

        # Running as root may create lock files that
        # need to be owned by firebird instead
        fixPermsConfigure

        if grep "^ *ISC_PASSWORD=" $DBAPasswordFile > /dev/null;
        then
            # Update existing line

            # create .tmp file preserving permissions
            cp -a "$DBAPasswordFile" "$DBAPasswordFile.tmp"

            sed -e "s/^ *ISC_PASSWORD=.*/ISC_PASSWORD=\"$NewPassword\"/" \
            < "$DBAPasswordFile" > "$DBAPasswordFile.tmp"
            mv -f "$DBAPasswordFile.tmp" "$DBAPasswordFile"
        else
            # Add new line
            echo "ISC_PASSWORD=$NewPassword" >> $DBAPasswordFile
        fi

        ISC_PASSWORD=$NewPassword
    fi
}

checkServerEnabled()
{
    local FB_FLAVOUR=$1
    local FB_VER=$2
    local ENABLE_SUPER_SERVER
    local DEFAULT

    SERVER_ENABLED="no"

    case "$FB_FLAVOUR" in
        classic)
            if egrep -q '^\s*disable\s*=\s*(yes|true|1)\s*(#.*)?$' /etc/xinetd.d/firebird$FB_VER_no_dots; then
                return 1 # false, disabled
            else
                return 0 # ok, true, enabled
            fi
        ;;

        super)
            DEFAULT="/etc/default/firebird${FB_VER}-${FB_FLAVOUR}"
            ENABLE_SUPER_SERVER="no"

            [ -r "$DEFAULT" ] && . "$DEFAULT"

            if [ "$ENABLE_SUPER_SERVER" = "yes" ]; then
                return 0
            else
                return 1
            fi
        ;;

        *)
            echo "Unknown FB_FLAVOUR ($FB_FLAVOUR). I support only 'super' or 'classic'."
            exit 1
        ;;
    esac
}

enable_firebird_pkg()
{
    set_firebird_pkg_enabled $1 yes
}

disable_firebird_pkg()
{
    set_firebird_pkg_enabled $1 no
}

set_firebird_pkg_enabled()
{
    local VARIANT=$1

    local FLAVOUR=${VARIANT##*-}
    local VER=${VARIANT##firebird}
    local VER=${VER%%-$FLAVOUR}

    local ENABLED=$2

    local VER_no_dots=`echo $VER | sed -e 's/\.//g'`

    case $ENABLED in
        yes) local DISABLED=no  ;;
        no)  local DISABLED=yes ;;
        *)
            echo "Unsupported ENABLED='$ENABLED'"
            exit 1
        ;;
    esac

    if [ "$ENABLED" = "yes" ] && checkServerEnabled $FLAVOUR $VER; then
        if [ "$FLAVOUR" = "super" ]; then
            call_initd firebird$VER-super start
        fi
        return 0
    fi
    if [ "$ENABLED" = "no" ] && ! checkServerEnabled $FLAVOUR $VER; then
        if [ "$FLAVOUR" = "super" ]; then
            call_initd firebird$VER-super stop
        fi
        return 0
    fi

    case $FLAVOUR in
        classic)
            local F=/etc/xinetd.d/firebird$VER_no_dots
            cp -a $F $F.tmp
            sed -e "s/^\\(\\s*disable\\s*=\\s*\\)\\(yes\\|no\\)\\(.*\\)/\\1$DISABLED\\3/g" \
                < $F > $F.tmp
            mv $F.tmp $F
            call_initd xinetd reload
        ;;
        super)
            call_initd "firebird$VER-super" stop
            local DEFAULT=/etc/default/firebird$VER-super
            if [ -f $DEFAULT ] && grep -q "^ENABLE_SUPER_SERVER=" $DEFAULT;
            then
                cp -a $DEFAULT $DEFAULT.tmp
                sed -e "s/^ENABLE_SUPER_SERVER=[^ #]\+/ENABLE_SUPER_SERVER=$ENABLED/g" \
                    < $DEFAULT > $DEFAULT.tmp
                mv -f $DEFAULT.tmp $DEFAULT
            else
                cat <<_EOF > $DEFAULT
# default file for firebird $VER super-server

# CAUTION: allowing two different server instances access the same database
#          simultaneously will inevitably lead to database corruption

# valid values are "yes" and "no"

ENABLE_SUPER_SERVER=$ENABLED
_EOF
            fi
            if [ "$ENABLED" = 'yes' ]; then
                call_initd "firebird$VER-super" start
            fi
        ;;
        *)
            echo Unsupported flavour "$FLAVOUR"
            exit 1
        ;;
    esac
}

remove_from_available_versions()
{
    local QUESTION="shared/firebird/active_version"
    # remove ourselves from owners
    db_unregister $QUESTION || true
    if db_get $QUESTION; then
        db_metaget $QUESTION owners
        OWNERS="$RET"
        db_subst $QUESTION choices "none, $OWNERS"

        # we were the one?
        if checkServerEnabled $FB_FLAVOUR $FB_VER; then
            disable_firebird_pkg "firebird$FB_VER-$FB_FLAVOUR"

            db_fset $QUESTION seen false
            db_input high $QUESTION || true
            db_go

            db_get $QUESTION || true
            if [ "$RET" != 'none' ]; then
                enable_firebird_pkg "$RET"
            fi
        fi
    fi
}

askForDBAPassword ()
{
    local VARIANT=$1

    local FLAVOUR=${VARIANT##*-}
    local VER=${VARIANT##firebird}
    local VER=${VER%%-$FLAVOUR}

    local DBAPasswordFile="/etc/firebird/$VER/SYSDBA.password"
    if [ -f $DBAPasswordFile ];
    then
        . $DBAPasswordFile
    fi

    QUESTION=shared/firebird/sysdba_password/new_password

    db_get "$QUESTION" || true
    if [ -z "$RET" ];
    then
        if [ -z "${ISC_PASSWORD:-}" ];
        then
            NewPassword=`cut -c 1-8 /proc/sys/kernel/random/uuid`
        else
            NewPassword=$ISC_PASSWORD
        fi
    else
        NewPassword=$RET
    fi

    writeNewPassword $NewPassword $VER

    # Make debconf forget the password
    db_reset $QUESTION || true
}


#-----------------------------------------------------------------------
# update inetd service entry 
# Check to see if we have xinetd installed or plain inetd. Install differs
# for each of them

updateInetdServiceEntry() {

    update-inetd --add \
      "gds_db\t\tstream\ttcp\tnowait\tfirebird\t/usr/sbin/tcpd\t$FB/bin/fb_inet_server"

    # No need to reload inetd, since update-inetd already reloads it
}

checkFirebirdAccount() {

    adduser --system --quiet --shell /bin/bash --home $VAR \
        --group --gecos "Firebird Database Administator" firebird
}

call_initd()
{
    script=$1
    action=$2

    if [ -f "/etc/init.d/$script" ];
    then
        if [ -x "`which invoke-rc.d 2>/dev/null`" ];
        then
            invoke-rc.d $script $action
        else
            /etc/init.d/$script $action
        fi
    fi
}

#---------------------------------------------------------------------------
# stop super server if it is running
# Also will only stop firebird, since that has the init script
# (firebird1.0.x deb has it)

stopServerIfRunning()
{

    # We conflict with previous firebid2-*-server packages
    # therefore there is no need to stop them

    #call_initd_script firebird stop
    #call_initd_script firebird2 stop
    call_initd_script "firebird$FB_VER" stop
}


#---------------------------------------------------------------------------
# stop server if it is running 

checkIfServerRunning() {

    stopServerIfRunning || exit $?

    ### These below are commented due to two reasons:
    ### 1) to avoid pre-dependency on procps
    ### 2) stopServerIfRunning (init.d script actually) must exit with
    ###    an error in case it was unable to stop the server anyway
    ### Classic installs are allowed to continue running whatever they're
    ### running until client disconnects
    ### What happend when new fb_inet_server works with previous fb_lock_mgr?
    ### We hope for the best, that's what.

#    # check if server is being actively used.
#    checkString=`ps -efww| egrep "(fbserver|fbguard)" |grep -v grep`
#    
#    if [ ! -z "$checkString" ]; then
#        echo "An instance of the Firebird Super server seems to be running."
#        echo "(the fbserver or fbguard process was detected running on your system)"
#        echo "Please quit all Firebird applications and then proceed"
#        exit 1
#    fi
#    
#    
#    checkString=`ps -efww| egrep "(fb_inet_server|gds_pipe)" |grep -v grep`
#    
#    if [ ! -z "$checkString" ]; then
#	echo "An instance of the Firebird classic server seems to be running."
#	echo "(the fb_inet_server or gds_pipe process was detected running on your system)"
#	echo "Please quit all Firebird applications and then proceed"
#	exit 1
#    fi
#	
#    # the following check for running interbase or firebird 1.0 servers.
#    checkString=`ps -efww| egrep "(ibserver|ibguard)" |grep -v grep`
#    
#    if [ ! -z "$checkString" ]; then
#	echo "An instance of the Firebird/InterBase Super server seems to be running."
#	echo "(the ibserver or ibguard process was detected running on your system)"
#	echo "Please quit all Firebird applications and then proceed"
#	exit 1
#    fi
#
#
#    checkString=`ps -efww| egrep "(ib_inet_server|gds_pipe)" |grep -v grep`
#    
#    if [ ! -z "$checkString" ]; then
#	echo "An instance of the Firebird/InterBase classic server seems to be running."
#	echo "(the fb_inet_server or gds_pipe process was detected running on your system)"
#	echo "Please quit all Firebird applications and then proceed"
#	exit 1
#    fi
#

    # stop lock manager if it is the only thing running.
#    for i in `ps -efww | egrep "[gds|fb]_lock_mgr" awk '{print $2}' ` 
#    do
#	kill $i
#    done

}

instantiate_security_db()
{
    SYS_DIR="$VAR/system"
    DEF_SEC_DB="$SYS_DIR/default-security.fdb"
    SEC_DB="$SYS_DIR/security.fdb"

    if ! [ -e "$SEC_DB" ];
    then
        install -o firebird -g firebird -m 0660 "$DEF_SEC_DB" "$SEC_DB"

        # Since we've copied the default security database, the SYSDBA password
        # must be reset
        if [ -f "$DBAPasswordFile" ]; then
            rm "$DBAPasswordFile"
        fi
        echo Created default security.fdb
    fi
}

firebird_config_postinst()
{
    instantiate_security_db

    fixPermsConfigure

    db_get shared/firebird/deactivate_version
    if [ -n "$RET" ] && [ "$RET" != "none" ]; then
        disable_firebird_pkg $RET
        db_reset shared/firebird/deactivate_version
    fi

    db_get shared/firebird/active_version
    if [ -n "$RET" ] && [ "$RET" != "none" ]; then
        enable_firebird_pkg $RET
        askForDBAPassword $RET
    fi

    debhelper_hook configure
}

# vi: set sw=4 ts=8 filetype=sh sts=4 :
