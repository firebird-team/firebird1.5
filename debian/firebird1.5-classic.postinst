#!/bin/sh
# postinst script for firebird1.5-classic

set -e
. /usr/share/debconf/confmodule
set -u

if [ -n "${DEBIAN_FIREBIRD_DEBUG:-}" ]; then
    set -x
fi
# summary of how this script can be called:
#        * <postinst> `configure' <most-recently-configured-version>
#        * <old-postinst> `abort-upgrade' <new version>
#        * <conflictor's-postinst> `abort-remove' `in-favour' <package>
#          <new-version>
#        * <deconfigured's-postinst> `abort-deconfigure' `in-favour'
#          <failed-install-package> <version> `removing'
#          <conflicting-package> <version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package
#
# quoting from the policy:
#     Any necessary prompting should almost always be confined to the
#     post-installation script, and should be protected with a conditional
#     so that unnecessary prompting doesn't happen if a package's
#     installation fails and the `postinst' is called with `abort-upgrade',
#     `abort-remove' or `abort-deconfigure'.

FB_VER=1.5
FB_FLAVOUR=classic
. /usr/share/firebird${FB_VER}-common/functions.sh

debhelper_hook()
{
    # This is here in order to make debhelper_hook() a valid
    # shell procedure in the case when debhelper has nothing
    # for us
    DuMmYnOtUsEd="dummy"

#DEBHELPER#
}

upgrade_from_pre_154()
{
    # move old SYSDBA.password and security.fdb in case we're upgrading
    # from version prior 1.5.4
    OldPasswordFile="/etc/firebird2/SYSDBA.password"
    SECURITY_DB=/var/lib/firebird/$FB_VER/system/security.fdb
    OLD_SECURITY_DB=/var/lib/firebird2/system/security.fdb
    if [ -f "$OldPasswordFile" ] && ! [ -f "$DBAPasswordFile" ] \
        && [ -f "$OLD_SECURITY_DB" ] && ! [ -f "$SECURITY_DB" ];
    then
        echo Moving pre-1.5.4 password file and security database
        mv "$OldPasswordFile" "$DBAPasswordFile"
        mv "$OLD_SECURITY_DB" "$SECURITY_DB"
    fi

    # Same for the log file
    OLD_LOG=/var/log/firebird.log
    if [ -f "$OLD_LOG" ] && ! [ -f "$LOG" ] ;
    then
        echo Moving pre-1.5.4 log file
        mv "$OLD_LOG" "$LOG"
    fi
}

case "$1" in
    configure)
        checkFirebirdAccount
        if [ -z "${2:-}" ]; then
            # first install. set DB dirs permissions
            fixPermsFirstInstall
        fi

        upgrade_from_pre_154

        firebird_config_postinst
    ;;

    *)
        debhelper_hook "$1"
    ;;
esac

exit 0

# vi: set sw=4 ts=8 filetype=sh sts=4 :
