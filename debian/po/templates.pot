# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: pkg-firebird-general@lists.alioth.debian.org\n"
"POT-Creation-Date: 2007-05-27 23:24+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: password
#. Description
#. Type: password
#. Description
#: ../server-templates.master:1001 ../server-templates.master:2001
msgid "Password for SYSDBA:"
msgstr ""

#. Type: password
#. Description
#. Type: password
#. Description
#: ../server-templates.master:1001 ../server-templates.master:2001
msgid ""
"Firebird has a special user named SYSDBA, which is the user that has access "
"to all databases. SYSDBA can also create new databases and users. Because of "
"this, it is necessary to secure SYSDBA with a password."
msgstr ""

#. Type: password
#. Description
#. Type: password
#. Description
#: ../server-templates.master:1001 ../server-templates.master:2001
msgid ""
"The password is stored in /etc/firebird/${FB_VER}/SYSDBA.password (readable "
"only by root). You may modify it there (don't forget to update the security "
"database too, using the gsec utility), or you may use dpkg-reconfigure to "
"update both."
msgstr ""

#. Type: password
#. Description
#: ../server-templates.master:1001
msgid ""
"If you don't enter a password, a random one will be used (and stored in "
"SYSDBA.password)."
msgstr ""

#. Type: password
#. Description
#: ../server-templates.master:2001
msgid "To keep your existing password, leave this blank."
msgstr ""

#. Type: select
#. Description
#: ../server-templates.master:3001
msgid "Enabled firebird version:"
msgstr ""

#. Type: select
#. Description
#: ../server-templates.master:3001
msgid ""
"Several firebird versions may be installed, but only one may be enabled and "
"running at any given time. This is because if two servers access the same "
"database simultaneously, the result is inevitably database corruption."
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:6001
msgid "Delete password database?"
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:6001
msgid ""
"The last package that uses password database at /var/lib/firebird/${FB_VER}/"
"system/security.fdb is being purged."
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:6001
msgid ""
"Leaving security database may present security risk. It is a good idea to "
"remove it if you don't plan re-installing firebird${FB_VER}."
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:6001
msgid ""
"The same stands for /etc/firebird/${FB_VER}/SYSDBA.password, where the "
"password for SYSDBA is kept."
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:7001
msgid "Delete databases from /var/lib/firebird/${FB_VER}/data?"
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:7001
msgid ""
"You may want to delete all databases from firebird standard database "
"directory, /var/lib/firebird/${FB_VER}/data. If you choose this option, all "
"files ending with \".fdb\" and \".fbk\" from the above directory and its "
"subdirectories will be removed."
msgstr ""

#. Type: boolean
#. Description
#: ../server-templates.master:7001
msgid ""
"Note that any databases outside of /var/lib/firebird/${FB_VER}/data will not "
"be affected."
msgstr ""

#. Type: error
#. Description
#: ../server-templates.master:8001
msgid "firebird${FB_VER}-${FB_FLAVOUR} server is in use"
msgstr ""

#. Type: error
#. Description
#: ../server-templates.master:8001
msgid ""
" To ensure data integrity, package removal/upgrade is aborted. Please stop "
"all local and remote clients before removing or upgrading firebird${FB_VER}-"
"${FB_FLAVOUR}"
msgstr ""

#. Type: title
#. Description
#: ../server-templates.master:9001
msgid "Password for ${PACKAGE}"
msgstr ""
