#!/bin/sh

set -e

if [ ! -f debian/control.in ] ;
then
    echo Run from wrong directory: "$PWD"
    exit 1
fi


if [ -f Makefile ];
then
    make --no-builtin-rules clean
fi

rm -rf	temp autom4te.cache gen

rm -rf	Makefile config.log config.status libtool

rm -rf	extern/editline/np/unvis.o_a \
	extern/editline/np/fgetln.o_a \
	extern/editline/np/strlcat.o_a \
	extern/editline/np/vis.o_a \
	extern/editline/np/strlcpy.o_a \
	extern/editline/editline.o_a \
	extern/editline/history.o_a \
	extern/editline/readline.o_a \
	extern/editline/tokenizer.o_a \
	extern/editline/libedit.a \
	extern/editline/Makefile \
	extern/editline/config.cache \
	extern/editline/config.h \
	extern/editline/config.log \
	extern/editline/config.status \
	\
	extern/editline/common.h \
	extern/editline/editline.c \
	extern/editline/editline.d \
	extern/editline/emacs.h \
	extern/editline/fcns.c \
	extern/editline/fcns.h \
	extern/editline/help.c \
	extern/editline/help.h \
	extern/editline/history.d \
	extern/editline/np/fgetln.d \
	extern/editline/np/strlcat.d \
	extern/editline/np/strlcpy.d \
	extern/editline/np/unvis.d \
	extern/editline/np/vis.d \
	extern/editline/readline.d \
	extern/editline/tokenizer.d \
	extern/editline/vi.h

rm -rf	extern/icu/source/bin \
	extern/icu/source/lib

rm -f	extern/icu/source/test/thaitest/Makefile \
	extern/icu/source/test/letest/Makefile \
	extern/icu/source/test/testmap/Makefile \
	extern/icu/source/test/threadtest/Makefile \
	extern/icu/source/test/threadtest/Makefile \
	extern/icu/source/extra/scrptrun/Makefile \
	extern/icu/source/tools/dumpce/Makefile \
	extern/icu/source/samples/layout/Makefile

#	src/dsql/dsql.tab.h

rm -rf	src/dsql/dsql.tab.c \
	src/dsql/parse.cpp \
        src/dsql/dsql.tab.h \
	src/include/gen/autoconfig.h \
	src/include/gen/blrtable.h \
	src/v5_examples/Makefile

rm -f 	src/burp/backup.cpp \
	src/burp/restore.cpp \
	src/gpre/gpre_meta.cpp \
	src/jrd/build_no.h \
	src/jrd/codes.cpp \
	src/msgs/build_file.cpp \
	src/dsql/array.cpp \
	src/dsql/blob.cpp \
	src/dsql/metd.cpp \
	src/dudley/exe.cpp \
	src/isql/extract.cpp \
	src/isql/isql.cpp \
	src/isql/show.cpp \
	src/jrd/dfw.cpp \
	src/jrd/dpm.cpp \
	src/jrd/dyn.cpp \
	src/jrd/dyn_def.cpp \
	src/jrd/dyn_del.cpp \
	src/jrd/dyn_mod.cpp \
	src/jrd/dyn_util.cpp \
	src/jrd/fun.cpp \
	src/jrd/grant.cpp \
	src/jrd/ini.cpp \
	src/jrd/met.cpp \
	src/jrd/pcmet.cpp \
	src/jrd/scl.cpp \
	src/msgs/change_msgs.cpp \
	src/msgs/check_msgs.cpp \
	src/msgs/enter_msgs.cpp \
	src/msgs/modify_msgs.cpp \
	src/qli/help.cpp \
	src/qli/meta.cpp \
	src/qli/proc.cpp \
	src/qli/show.cpp \
	src/utilities/security.cpp

rm -f src/*.fdb src/*.lnk src/indicator.* src/Makefile

exit 0

