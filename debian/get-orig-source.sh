#!/bin/sh

set -e
set -u

TMPDIR=`mktemp -d -p .`

trap "rm -rf $TMPDIR" INT QUIT 0

VER="1.5.4.4910rel"
# the checksum of the upstream-provider tar.bz2
MD5SUM="6655190f760217845623a75001b6ddd4"
UPSTREAM_TAR=firebird-1.5.4.4910.tar.bz2
UPSTREAM_DIR=firebird-1.5.4.4910
#URL=http://www.firebirdsql.org/download/prerelease/$UPSTREAM_TAR
URL=http://surfnet.dl.sourceforge.net/sourceforge/firebird/$UPSTREAM_TAR
ORIG="../firebird1.5_$VER.orig.tar.gz"
ORIG_DIR="firebird1.5-$VER.orig"
# the checksum of the resulting orig.tar.gz
ORIG_MD5="a63901c2c081710e879c3474c4baa0d4"

if [ -e "$ORIG" ]; then
    echo "$ORIG already exists."
    MD5=`md5sum $ORIG | cut -d " " -f 1`
    if [ "$MD5" = "$ORIG_MD5" ]; then
        echo "MD5 checksum matches."
    else
        echo "MD5 checksum does not match!"
    fi
    echo "Aborting."
    exit 1
fi

wget -O $TMPDIR/$UPSTREAM_TAR $URL

UPSTREAM_MD5=`md5sum $TMPDIR/$UPSTREAM_TAR | cut -d " " -f 1`

if [ "$MD5SUM" != "$UPSTREAM_MD5" ];
then
    echo "Upstream tar md5sum differs from expected. Found $UPSTREAM_MD5; expected $MD5SUM"
    exit 1
fi

echo MD5 checksum OK

echo -n Recompressing with gzip...
bzcat $TMPDIR/$UPSTREAM_TAR | gzip > $ORIG
echo " done."
